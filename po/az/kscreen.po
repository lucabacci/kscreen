# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kscreen package.
#
# Kheyyam <xxmn77@gmail.com>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: kscreen\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-01 23:30+0000\n"
"PO-Revision-Date: 2022-07-06 17:38+0400\n"
"Last-Translator: Kheyyam <xxmn77@gmail.com>\n"
"Language-Team: Azerbaijani <kde-i18n-doc@kde.org>\n"
"Language: az\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.04.2\n"

#: daemon.cpp:125
msgid "Switch Display"
msgstr "Ekranı dəyişin"

#~ msgid "Switch to external screen"
#~ msgstr "Xarici ekrana keçin"

#~ msgid "Switch to laptop screen"
#~ msgstr "Noutbuk ekranına keçin"

#~ msgid "Unify outputs"
#~ msgstr "Çıxışları birləşdirin"

#~ msgid "Extend to left"
#~ msgstr "Sola genişləndirin"

#~ msgid "Extend to right"
#~ msgstr "Sağa genişləndiirn"

#~ msgid "Leave unchanged"
#~ msgstr "Dəyişdirmədən çıxın"
