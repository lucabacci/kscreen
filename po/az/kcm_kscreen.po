# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kscreen package.
#
# Kheyyam <xxmn77@gmail.com>, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: kscreen\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-21 02:29+0000\n"
"PO-Revision-Date: 2023-02-17 01:07+0400\n"
"Last-Translator: Kheyyam <xxmn77@gmail.com>\n"
"Language-Team: Azerbaijani <kde-i18n-doc@kde.org>\n"
"Language: az\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.12.2\n"

#: output_model.cpp:98
#, kde-format
msgid "%1 Hz"
msgstr "%1 Hz"

#: output_model.cpp:633
#, kde-format
msgctxt "Width x height"
msgid "%1x%2"
msgstr ""

#: output_model.cpp:652
#, kde-format
msgctxt "Width x height (aspect ratio)"
msgid "%1x%2 (%3:%4)"
msgstr "%1x%2 (%3:%4)"

#: output_model.cpp:733
#, kde-format
msgid "None"
msgstr "Heç biri"

#: output_model.cpp:740
#, kde-format
msgid "Replicated by other output"
msgstr "Başqa çıxışlar ilə əvəzlənib"

#: package/contents/ui/main.qml:40
#, kde-format
msgid "Keep display configuration?"
msgstr "Ekran tənzimlənməsi saxlanılsın?"

#: package/contents/ui/main.qml:55
#, kde-format
msgid "Will revert to previous configuration in %1 second."
msgid_plural "Will revert to previous configuration in %1 seconds."
msgstr[0] "%1 saniyəyə əvvəlki tənzimləmələrə qaytarılacaq."
msgstr[1] "%1 saniyəyə əvvəlki tənzimləməyə qaytarılacaq."

#: package/contents/ui/main.qml:72
#, kde-format
msgid "&Keep"
msgstr "&Saxlayın"

#: package/contents/ui/main.qml:85
#, kde-format
msgid "&Revert"
msgstr "&Geri qaytarın"

#: package/contents/ui/main.qml:105 package/contents/ui/main.qml:287
#, kde-format
msgctxt "@info"
msgid "All displays are disabled. Enable at least one."
msgstr ""

#: package/contents/ui/main.qml:107 package/contents/ui/main.qml:289
#, kde-format
msgctxt "@info"
msgid ""
"Gaps between displays are not supported. Make sure all displays are touching."
msgstr ""

#: package/contents/ui/main.qml:119 package/contents/ui/main.qml:302
#, kde-format
msgid "A new output has been added. Settings have been reloaded."
msgstr "Yeni çıxış kanalı əlavə olundu. Ayarlar yeniləndi."

#: package/contents/ui/main.qml:121 package/contents/ui/main.qml:304
#, kde-format
msgid "An output has been removed. Settings have been reloaded."
msgstr "Çıxış kanalı silindi. Ayarlar yeniləndi."

#: package/contents/ui/main.qml:171
#, kde-format
msgid "No KScreen backend found. Please check your KScreen installation."
msgstr ""
"Heç bir KScreen modulu tapılmadı. Lütfən KScreen quraşdırıldığını yoxlayın."

#: package/contents/ui/main.qml:181
#, kde-format
msgid "Outputs could not be saved due to error."
msgstr "Çıxışlar xəta olduğu müddətdə saxlanıla bilməz."

#: package/contents/ui/main.qml:191
#, fuzzy, kde-format
#| msgid ""
#| "New global scale applied. Change will come into effect after restart."
msgid ""
"Global scale changes will come into effect after the system is restarted."
msgstr ""
"Yeni qlobal miqyas tətbiq edilib. Dəyişiklik, yenidən başladıqdan sonra "
"qüvvəyə minəcək."

#: package/contents/ui/main.qml:197
#, kde-format
msgid "Restart"
msgstr "Yenidən başladın"

#: package/contents/ui/main.qml:217
#, kde-format
msgid "Display configuration reverted."
msgstr "Ekran tənizmlənməsi geri qaytarıldı."

#: package/contents/ui/main.qml:225
#, kde-format
msgctxt "@title:window"
msgid "Change Priorities"
msgstr ""

#: package/contents/ui/main.qml:254 package/contents/ui/OutputPanel.qml:42
#, kde-format
msgid "Primary"
msgstr "Əsas"

#: package/contents/ui/main.qml:260
#, kde-format
msgid "Raise priority"
msgstr ""

#: package/contents/ui/main.qml:270
#, kde-format
msgid "Lower priority"
msgstr ""

#: package/contents/ui/Orientation.qml:12
#, kde-format
msgid "Orientation:"
msgstr "İstiqamət:"

#: package/contents/ui/Orientation.qml:26
#: package/contents/ui/OutputPanel.qml:154
#: package/contents/ui/OutputPanel.qml:195
#, kde-format
msgid "Automatic"
msgstr "Avtomatik"

#: package/contents/ui/Orientation.qml:35
#, kde-format
msgid "Only when in tablet mode"
msgstr "Yalnız planşet rejimində"

#: package/contents/ui/Orientation.qml:44
#, kde-format
msgid "Manual"
msgstr "Əl ilə"

#: package/contents/ui/Output.qml:238
#, kde-format
msgid "Replicas"
msgstr "Replikalar"

#: package/contents/ui/OutputPanel.qml:24
#, kde-format
msgid "Enabled"
msgstr "İşə salınıb"

#: package/contents/ui/OutputPanel.qml:35
#, kde-format
msgid "Change Screen Priorities…"
msgstr ""

#: package/contents/ui/OutputPanel.qml:48
#, fuzzy, kde-kuit-format
#| msgctxt "@info"
#| msgid ""
#| "This determines which screen your main desktop and panel appear on. Some "
#| "older games also use it to decide which screen to appear on.<nl/><nl/>It "
#| "has no effect on what screen notifications or other windows appear on."
msgctxt "@info"
msgid ""
"This determines which screen your main desktop appears on, along with any "
"Plasma Panels in it. Some older games also use this setting to decide which "
"screen to appear on.<nl/><nl/>It has no effect on what screen notifications "
"or other windows appear on."
msgstr ""
"Bu. əsas iş masanızın hansı ekranının və panelinin göstəriləcəyini aşkar "
"edir. Bəzi köhnə oyunlar hansı ekranda göstəriləcəyinə qərar vermək üçün "
"bunu istifadə edir.<nl/><nl/>Bu, hansı ekran bilirişlərinin və ya digər "
"pəncərələrin göstəriləcəyinə heç bir təsir etmir."

#: package/contents/ui/OutputPanel.qml:53
#, kde-format
msgid "Resolution:"
msgstr "Görüntü icazələri:"

#: package/contents/ui/OutputPanel.qml:73
#, fuzzy, kde-kuit-format
#| msgctxt "@info"
#| msgid ""
#| "\"%1\" is the only resolution supported by this display.<nl/><nl/>Using "
#| "unsupported resolutions was possible in the Plasma X11 session, but they "
#| "were never guaranteed to work and are not available in this Plasma "
#| "Wayland session."
msgctxt "@info"
msgid ""
"&quot;%1&quot; is the only resolution supported by this display.<nl/><nl/"
">Using unsupported resolutions was possible in the Plasma X11 session, but "
"they were never guaranteed to work and are not available in this Plasma "
"Wayland session."
msgstr ""
"\"%1\", bu ekran tərəfindən dəstəklənən yeganə icazədir.<nl/><nl/"
">Dəstəklənməyən icazələrin istifadə olunması Plasma X11 sessiyasında "
"mümkündür, lakin onun Plasma Wayland sessiyasında işləməsinə və əlçatan "
"olmasına heç bir zəmanət verilmir."

#: package/contents/ui/OutputPanel.qml:83
#, kde-format
msgid "Scale:"
msgstr "Miqyas:"

#: package/contents/ui/OutputPanel.qml:112 package/contents/ui/Panel.qml:107
#, kde-format
msgctxt "Global scale factor expressed in percentage form"
msgid "%1%"
msgstr "%1%"

#: package/contents/ui/OutputPanel.qml:124
#, kde-format
msgid "Refresh rate:"
msgstr "Yenilənmə tezliyi:"

#: package/contents/ui/OutputPanel.qml:144
#, kde-format
msgid "\"%1\" is the only refresh rate supported by this display."
msgstr "\"%1\" bu ekran tərəfindən dəstəklənən yeganə yenilənmə aralığıdır."

#: package/contents/ui/OutputPanel.qml:149
#, kde-format
msgid "Adaptive sync:"
msgstr "Uyğunlaşdırılmış eyniləşdirmə:"

#: package/contents/ui/OutputPanel.qml:152
#, kde-format
msgid "Never"
msgstr "Heç vaxt"

#: package/contents/ui/OutputPanel.qml:153
#, kde-format
msgid "Always"
msgstr "Həmişə"

#: package/contents/ui/OutputPanel.qml:165
#, kde-format
msgid "Overscan:"
msgstr "Sahə:"

#: package/contents/ui/OutputPanel.qml:189
#, kde-format
msgid "RGB Range:"
msgstr "RGB aralığı:"

#: package/contents/ui/OutputPanel.qml:196
#, kde-format
msgid "Full"
msgstr "Tam"

#: package/contents/ui/OutputPanel.qml:197
#, kde-format
msgid "Limited"
msgstr "Məhdud"

#: package/contents/ui/OutputPanel.qml:213
#, kde-format
msgid "Replica of:"
msgstr "Replika mənbəyi:"

#: package/contents/ui/Panel.qml:29
#, kde-format
msgid "Device:"
msgstr "Cihaz:"

#: package/contents/ui/Panel.qml:71
#, kde-format
msgid "Global scale:"
msgstr "Qlobal miqyas:"

#: package/contents/ui/Panel.qml:131 package/contents/ui/Panel.qml:151
#, kde-format
msgid "Legacy Applications (X11):"
msgstr "Köhnə tətbiqlər (X11):"

#: package/contents/ui/Panel.qml:136
#, kde-format
msgctxt "The apps themselves should scale to fit the displays"
msgid "Apply scaling themselves"
msgstr "Öz-özünə genişlənməni tətbiq edin"

#: package/contents/ui/Panel.qml:141
#, kde-format
msgid ""
"Legacy applications that support scaling will use it and look crisp, however "
"those that don't will not be scaled at all."
msgstr ""
"Miqyaslama, miqyaslamanı dəstəkləyən köhnə tətbiqlər üçün istifadə olunacaq, "
"Miqyaslanma nəzərdə tutulmayan pəncərələr ümumiyyətlə miqyaslanmayacaq."

#: package/contents/ui/Panel.qml:152
#, kde-format
msgctxt "The system will perform the x11 apps scaling"
msgid "Scaled by the system"
msgstr "Sistemə görə miqyaslanıb"

#: package/contents/ui/Panel.qml:157
#, kde-format
msgid ""
"All legacy applications will be scaled by the system to the correct size, "
"however they will always look slightly blurry."
msgstr ""
"Bütün köhnə tətbiqlər sistemə uyğun olaraq düzgün ölçüsünə miqyaslanacaq, "
"bununla belə onlar azca bulanıq görünəcək."

#: package/contents/ui/Panel.qml:171
#, kde-format
msgid ""
"The global scale factor is limited to multiples of 6.25% to minimize visual "
"glitches in applications using the X11 windowing system."
msgstr ""
"X11 pəncərə sistemindən istifadə edən tətbiqlərdə vizual xətaları minimuma "
"endirmək üçün qlobal miqyas faktoru 6,25% ilə məhdudlaşır."

#: package/contents/ui/RotationButton.qml:50
#, kde-format
msgid "90° Clockwise"
msgstr "90° saat əqrəbi istiqamətinə"

#: package/contents/ui/RotationButton.qml:54
#, kde-format
msgid "Upside Down"
msgstr "Baş a.aşağı"

#: package/contents/ui/RotationButton.qml:58
#, kde-format
msgid "90° Counterclockwise"
msgstr "90° saat ərəbinin əksinə"

#: package/contents/ui/RotationButton.qml:63
#, kde-format
msgid "No Rotation"
msgstr "Dönməsiz"

#: package/contents/ui/Screen.qml:48
#, kde-format
msgid "Drag screens to re-arrange them"
msgstr "Ekranların yerini dəyişərək düzün"

#: package/contents/ui/Screen.qml:61
#, kde-format
msgid "Identify"
msgstr "Müəyyən edin"

#~ msgid "Save displays' properties:"
#~ msgstr "Ekran xüsusiyyətləri yadda saxlanılsın:"

#~ msgid "For any display arrangement"
#~ msgstr "İstənilən ekran düzülüşü üçün"

#~ msgid "For only this specific display arrangement"
#~ msgstr "Yalnız xüsusi ekran düzülüşü üçün"

#~ msgid ""
#~ "Are you sure you want to disable all outputs? This might render the "
#~ "device unusable."
#~ msgstr ""
#~ "Bütün çıxışları söndürmək istəsədiyinizə əminsiniz? Bu cihazı qeyri-işlək "
#~ "hala sala bilər."

#~ msgid "Disable All Outputs"
#~ msgstr "Bütün çıxışları söndürün"

#~ msgctxt "@info"
#~ msgid ""
#~ "Determines how much padding is put around the image sent to the display"
#~ msgstr ""
#~ "Ekrana göndərilən təsvirin ətrafında nə qədər boşluğun olacağını müəyyən "
#~ "edir"

#~ msgctxt "@info"
#~ msgid ""
#~ "Determines whether or not the range of possible color values needs to be "
#~ "limited for the display."
#~ msgstr ""
#~ "Ekran üçün mümkün rəng dəyərləri aralığının məhdudlaşdırılıb-"
#~ "məhdudlaşdırılmayacağını müəyyən edir."
